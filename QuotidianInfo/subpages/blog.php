<?php
	echo "<div class='element'>\n";
	echo "	<div class='header'>\n";
	echo "    <span class='title'>\n";
	echo "		" . $row['title'] . "\n";
	echo "    </span>";
	echo "    <span class='date'>\n";
	echo "		" . $row['date'] . "\n";
	echo "    </span>\n";
	echo "	</div>\n";
	echo "	<div class='body'>\n";
	echo "		" . $row['data'] . "\n";
	echo "	</div>\n";
	echo "	<div class='footer'>\n";
	echo "	</div>\n";
	echo "</div>\n";
?>